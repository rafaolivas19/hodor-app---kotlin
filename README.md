# Hodor App - Kotlin

Esta librería sirve como un ejemplo de proyecto Kotlin con estructura de multi proyectos de Gradle (ver https://guides.gradle.org/creating-multi-project-builds/).

Consiste en una aplicación que cuenta con un submodulo a la librería Hodor de ejemplo (https://gitlab.com/rafaolivas19/hodor-library---kotlin) y que ejecuta su único método `speak()` de la clase `org.rafaolivas.Hodor`, de tal modo que el output es `hodor`.

---

## Compilación

Para construir el proyecto es necesario contar con lo siguiente:

- JDK 8 ó mayor.
- Gradle 5.0 ó mayor.

Después seguir los siguientes pasos:

1. En la raíz del proyecto, ejecutar el comando desde terminal `gradle build`.
1. Esto generará nuevos directorios. El archivo `.jar` puede encontrarse en la ruta `app/build/libs/`.
1. Se puede probar su funcionamiento ejecutando el `.jar` con el comando `java -jar ./app-0.1.0.jar`.

---

## Ejecución

Se puede ejecutar el proyecto con el comando `gradle run` desde la raíz del proyecto.